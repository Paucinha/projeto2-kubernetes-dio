# Projeto Criando um Pipelene de Deploy com Gitlab e Kubernetes

[![GitLab](https://img.shields.io/gitlab/license/41912814)](https://gitlab.com/Paucinha/projeto2-kubernetes-dio/-/blob/main/LICENSE)

# Descrição
Neste projeto foi criado um pipeline de deploy de uma aplicação com cenários de produção de imagens com Docker e criação dos deployments em um cluster kubernetes em nuvem utilizando o GCP.

##

<div style="display: inline_block"><br>  
  <img align="center" alt="kubernetes" height="80" width="90" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/kubernetes/kubernetes-plain.svg">
<img align="center" alt="mysql" height="80" width="90 "src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-plain-wordmark.svg" />
</div>
                                                                        
##    

# Arquitetura do Cluster e Aplicação

![Arquitetura do cluster](https://raw.githubusercontent.com/Paucinha/assets/master/Arquitetura%20do%20cluster%20e%20aplica%C3%A7%C3%A3o2.jpg)

# Banco de Dados

```bash
CREATE TABLE mensagens (
    id int,
    nome varchar(),
    email varchar(),
    comentario varchar()
);
```

Projeto desenvolvido durante o Bootcamp Cloud DevOps Experience - Banco Carrefour, fornecido pela [**DIO**](https://www.dio.me/)

By Paucinha!!!
